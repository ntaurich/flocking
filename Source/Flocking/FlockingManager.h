#pragma once

#include "CoreMinimal.h"
#include "FlockingManager.generated.h"

UCLASS()
class FLOCKING_API UFlockingManager : public UObject
{

public:
	GENERATED_BODY()

	void Init( UWorld *world, UStaticMeshComponent *mesh );
	void Flock(); 
	UPROPERTY(EditAnywhere)
	class APawn* Goal;

private:
	UWorld *World;	
	UPROPERTY(EditAnywhere)
	bool initialized;
	UPROPERTY(EditAnywhere)
	TArray<class AAgent *> Agents; //array stores all agents
	FVector RuleOne(AAgent* CurrBoid);
	FVector RuleTwo(AAgent* CurrBoid);
	FVector RuleThree(AAgent* CurrBoid);
	FVector Bound (AAgent* CurrBoid);
	FVector Follow (AAgent *CurrBoid);


};