Created With: Unreal 4.24.3
Created By: Nina Taurich

Controls:
W- Forward
S- Backward
A- Left
D- Right
Shift-Up
Space- Down
Goal: Try and lead all the boids to the cube before the timer runs out. 
The Boids only follow if you are close to them

YouTube Video of Gameplay: https://youtu.be/hEY_0pkbPpw